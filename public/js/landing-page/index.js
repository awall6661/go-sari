$(document).ready(function(){
    $('.count').counterUp({
      delay: 10,
      time: 1000
    });
  });


  window.addEventListener('load', function(){
    const banner = document.querySelector('.jumbotron-img');
    // const aboutImg = document.querySelector('.about-img');
   

    banner.classList.add('appear')
    // aboutImg.classList.add('appear')
})

AOS.init({
  offset:300,
  duration:1000,
});

jQuery( document ).ready( function($) {
 $( window ).scroll( function () {
   if ( $(document).scrollTop() > 100 ) {
     // Navigation Bar
     $('.custom-navbar').removeClass('animate_fadeIn');
     $('body').addClass('shrink');
     $('.custom-navbar').addClass('animate_fadeInDown');
   } else {
     $('.custom-navbar').removeClass('animate_fadeInDown');
     $('body').removeClass('shrink');
     $('.custom-navbar').addClass('animate_fadeIn');
   }
 });
});