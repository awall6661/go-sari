<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{{ $invoice->code }}</title>
  <style>
    body{
      
      margin: 0;
      padding: 0;
    }
    h1,h2,h4,h4,h5,h6{
      margin: 0;
      padding: 0;
    }
    p{
      margin: 0;
      padding: 0;
    }
    .container{
      width: 80%;
      margin-right: auto;
      margin-left: auto;
    }
    .brand-section{
       background-color: #08C559;
       padding: 20px 0;
    }
    .logo{
      width: 50%;
    }

    .row{
      display: flex;
      flex-wrap: wrap;
    }
    .col-6{
      width: 50%;
      flex: 0 0 auto;
    }
    .text-white{
      color: #fff;
    }
    .company-details{
      float: right;
      text-align: right;
    }
    .body-section{
      padding: 16px;
      border: 1px solid gray;
    }
    .sub-heading{
      color: #262626;
      margin-bottom: 05px;
    }
    table.table{
      background-color: #fff;
      width: 100%;
      border-collapse: collapse;
    }
    table.table thead tr{
      border: 1px solid #111;
      background-color: #f2f2f2;
    }
    table.table td {
      vertical-align: middle !important;
      text-align: center;
    }
    table.table th, table td {
      padding-top: 08px;
      padding-bottom: 08px;
    }
    .table-bordered{
      box-shadow: 0px 0px 5px 0.5px gray;
    }
    .table-bordered td, .table-bordered th {
      border: 1px solid #dee2e6;
    }
    .text-right{
      text-align: end;
    }
    .w-20{
      width: 20%;
    }
    .float-right{
      float: right;
    }
  </style>
</head>
<body>

  <div class="container">
    <div class="brand-section">
      <div class="row">
          <div class="col-6">
              <h2 class="text-white" style="margin-left:20px;">Tagihan Pembayaran</h2>
              <p class="text-white" style="margin-left:20px;">Bulan {{ $invoice->month }}</p>
          </div>
          <div class="col-6">
              <div class="company-details">
              </div>
          </div>
      </div>
  </div>

    <div class="body-section">
      <div class="">
        <table border="0">
          <tr>
            <td style="width: 70px;">Nama</td>
            <td>: {{$invoice->user->name}}</td>
          </tr>
          <tr>
            <td style="width: 70px;">Email</td>
            <td>: {{$invoice->user->email}}</td>
          </tr>
          <tr>
            <td style="width: 70px;">No. HP</td>
            <td>: {{$invoice->user->phone}}</td>
          </tr>
          <tr>
            <td style="width: 70px;">Alamat</td>
            <td>: {{$invoice->user->address}}</td>
          </tr>
        </table>

      </div>
    </div>

    <div class="body-section">
      <h4 class="heading">Rincian Tagihan</h4>
      <br>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No.</th>
            <th>Kode Invoice</th>
            <th class="w-20">Bulan</th>
            <th class="w-20">Batas Pembayaran</th>
            <th class="w-20">Total Tagihan</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td> 1. </td>
            <td class="text-center"> {{ $invoice->code }} </td>
            <td class="text-center"> {{ $invoice->month }} </td>
            <td class="text-center"> {{ date('d/m/Y H:i', strtotime($invoice->payment_due)) }} </td>
            <td class="text-right"> Rp. {{ number_format($invoice->total_amount) }} </td>
          </tr>
        </tbody>
      </table>
      <br>
      <!-- If paid -->
      @if($invoice->payment_status == "paid")
        <p class="heading">Status Pembayaran: Lunas </p>
        <p class="heading">Tanggal Pembayaran: {{date('d/m/Y, H:i', strtotime($invoice->payment_date))}} </h4>
        <p class="heading">Metode Pembayaran: Transfer Bank </p>
      @elseif($invoice->payment_status == "pending")
        <p class="heading">Status Pembayaran: Menunggu Konfirmasi </p>
      @else
        <p class="heading">Status Pembayaran: Belum Lunas </p>
      @endif
    </div>    
  </div>      

</body>
</html>