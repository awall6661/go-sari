@extends('layouts.member.dashboard.main')
@section('style')
<style>
  .avatar-option {
    transition: .5s ease;
    opacity: 1;
    position: absolute;
    top: 80%;
    right: 15%;
    text-align: center;
  }
  .avatar-option:hover {
    cursor: pointer;
    color: #6c757d;
  }
  /* .img-profil:hover img {
    opacity: 0.5;
  }
  .img-profil:hover .avatar-option {
    opacity: 1;
  } */
</style>
@endsection

@section('content')
<!-- Main Content -->
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Profil</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Profil</div>
      </div>
    </div>
    <div class="section-body">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-profil">
            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="profil-img">
                <form method="POST" enctype="multipart/form-data" id="upload_image_form" action="javascript:void(0)" >
                  @if($user->profile_picture == "avatar-default.png")
                  <img src="{{ asset('img/avatar-default.png') }}" id="profileImg" class="img-fluid element1 profile-pic" alt="" style="min-height:100px; min-width:100px" data-img="default" />
                  @else
                  <img src="{{ asset('img/member/avatar/'.$user->profile_picture) }}" id="profileImg" class="img-fluid element1 profile-pic" alt="" data-img="user" />
                  @endif   
                  
                  <input type="file"  class="form-control" name="profile_picture" id="inputFile" style="display:none;">
               
                  <button id="btnfile" class="btn btn-primary mt-3">Ubah Foto Profil</button>
                  <button type="submit" class="btn btn-primary mt-3 mr-2 ml-1" id="saveImg" style="display:none">Simpan</href>
                  <button class="btn btn-danger mt-3" id="cancelImg" style="display:none">Batal</href>
                </form>
                </div>
                <div class="profile mt-3">
                <div class="name">
                  <h4>{{ $user->name }}</h4>
                </div>
                <table>
                  <tr>
                    <td style="width: 60px; padding:10px 3px">Email</td>
                    <td>: {{ $user->email }}</td>
                  </tr>
                  <tr>
                    <td style="width: 60px; padding:10px 2px">NIK</td>
                    <td>: {{ $user->nik }}</td>
                  </tr>
                  <tr>
                    <td style="width: 60px; padding:10px 2px">No. HP</td>
                    <td>: {{ $user->phone }}</td>
                  </tr>
                  <tr>
                    <td style="width: 60px; padding:10px 2px">Dusun</td>
                    <td>: {{ $user->hamlet->hamlet_name }}</td>
                  </tr>
                  <tr>
                    <td style="width: 60px; padding:10px 2px">RT</td>
                    <td>: {{ $user->rt }}</td>
                  </tr>
                  <tr>
                    <td style="width: 60px; padding:2px">Alamat</td>
                    <td>: {{ $user->address }}</td>
                  </tr>
                </table>
                </div>
                
              </div>
              <div class="col-md-8 col-sm-12">
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
        <form method="post" class="needs-validation" novalidate="" action="{{ route('member.update') }}">@csrf
            <div class="card">
              <div class="card-header">
                <h4>Edit Profil</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="name" value="{{ $user->name }}" required="">
                  <div class="invalid-feedback">
                    Nama tidak boleh kosong!
                  </div>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" value="{{ $user->email }}" required="">
                  <div class="invalid-feedback">
                    Please fill in the email
                  </div>
                </div>
                <div class="form-group">
                  <label>Phone</label>
                  <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                </div>
                <div class="form-group">
                  <label for="hamlet" class="form-label">Dusun</label>
                  <select class="form-control" id="hamlet" name="hamlet_id">
                    @foreach ($hamlets as $hamlet)
                    <option value="{{ $hamlet->id }}" {{ $hamlet->id == $user->hamlet_id? 'selected': '' }}>{{ $hamlet->hamlet_name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="rt" class="form-label">RT</label>
                  <select class="form-control  @error('rt') is-invalid @enderror" id="rt" name="rt">
                    @foreach(range(1,10) as $rt)
                    <option value="{{ sprintf("%02d", $rt) }}" {{ $rt == $user->rt? 'selected': '' }}>{{ sprintf("%02d", $rt) }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="address" class="form-label">{{ __('Alamat Lengkap') }} <span
                      class="required">*</span></label>
                  <textarea name="address" class="form-control @error('address') is-invalid @enderror"
                    id="address" rows="2" style="height: 100px">{{ $user->address }}</textarea>
                  <div class="invalid-feedback">
                    Alamat tidak boleh kosong!
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="row mt-2 mt-sm-2">
        <div class="col-md-8 col-sm-12">
          
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){

  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
   });

  $("#btnfile").click(function (e) {
    $("#inputFile").click();
    e.preventDefault();
  });

  $("#inputFile").change(function () {
    $('#saveImg').show();
    $('#cancelImg').show();

    let reader = new FileReader();
    reader.onload = (e) => { 
      $('#profileImg').attr('src', e.target.result);
      $('#sidebarAvatar').attr('src', e.target.result);  
    }
    reader.readAsDataURL(this.files[0]); 
  });
  
  $('#cancelImg').click(function() {
    $(this).hide();
    $('#saveImg').hide();
    $('#inputFile').val('');

    let src =  $('#profileImg').data('img');

    if(src == "default") {
      $('#profileImg').attr('src', "{{ asset('img/avatar-default.png') }}"); 
    } else {
      $('#sidebarAvatar').attr('src', "{{ asset('img/member/avatar/'.$user->profile_picture) }}"); 
      $('#profileImg').attr('src', "{{ asset('img/member/avatar/'.$user->profile_picture) }}"); 
    }
  });
  

  $('#upload_image_form').submit(function(e) {
    e.preventDefault();

    var formData = new FormData(this);

    $.ajax({
      type:'POST',
      url: "{{ url('profil/update-avatar')}}",
      data: formData,
      cache:false,
      contentType: false,
      processData: false,
      success: (data) => {
          this.reset();
          iziToast.success({
            position: 'topRight',
            title: 'Sukses',
            message: data.message,
            timeout: 3000
          });
          $('#saveImg').hide();
          $('#cancelImg').hide();
      },
      error: function(data){
          console.log(data);
        }
      });
  });
});

</script>
  @if (Session::has('success'))
    <script>
      iziToast.success({
        position: 'topRight',
        title: 'Sukses',
        message: '{{ Session::get("success") }}',
        timeout: 3000
      });
    </script>
  @endif
  @if (Session::has('error'))
    <script>
      iziToast.error({
        position: 'topRight',
        title: 'Error',
        message: '{{ Session::get("error") }}',
        timeout: 3000
      });
    </script>
  @endif
@endsection