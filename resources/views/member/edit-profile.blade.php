@extends('layouts.app')

@section('content')

<div class="card-body">
    <form method="POST" action="{{route('member.update')}}">
        @method('patch')
        @csrf
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-3 border-right">
                    <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                        <img class="rounded-circle mt-5" src="{{asset('img/member/avatar/'.$user->profile_picture)}}">
                        <span class="font-weight-bold" name="name" value="">{{$user->name}}
                        </span>
                        <span class="text-black-50">{{$user->email}}</span><span> </span>
                    </div>
                </div>
                <div class="col-md-5 border-right">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Profile Settings</h4>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6"><label class="labels">Name</label><input type="text"
                                    class="form-control" name="name" placeholder="first name" value="{{$user->name}}"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">N I K</label>
                                <input type="text" name="nik" class="form-control" placeholder="enter phone number"
                                    value="{{$user->nik}}" required>
                                    <div class="invalid-feedback">
                                        Nama tidak boleh kosong!
                                      </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12"><label class="labels">Phone Number</label>
                                <input type="text" class="form-control" name="phone" placeholder="enter phone number"
                                    value="{{$user->phone}}" required>
                                    <div class="invalid-feedback">
                                        No telepon tidak boleh kosong!
                                      </div>
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Email</label>
                                <input type="text" class="form-control" value="{{$user->email}}">
                            </div>
                            <div class="col-md-12"><label class="labels">Alamat</label>
                                <input type="text" class="form-control" name="address" value="{{$user->address}}" required>
                                <div class="invalid-feedback">
                                    Alamat tidak boleh kosong!
                                  </div>
                            </div>
                        </div>
                        <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="submit">Save
                                Profile</button></div>
                    </div>
                </div>
                {{-- <div class="col-md-4">
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center experience"><span>Edit
                                Experience</span><span class="border px-3 p-1 add-experience"><i
                                    class="fa fa-plus"></i>&nbsp;Experience</span></div><br>
                        <div class="col-md-12"><label class="labels">Experience in Designing</label><input type="text"
                                class="form-control" placeholder="experience" value=""></div> <br>
                        <div class="col-md-12"><label class="labels">Additional Details</label><input type="text"
                                class="form-control" placeholder="additional details" value=""></div>
                    </div>
                </div> --}}
            </div>
        </div>
</div>
</div>

</form>
</div>
@endsection
