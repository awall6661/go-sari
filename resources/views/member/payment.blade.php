<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/member/payment/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon-icon.png') }}">
    <title>Go Sari - Unit Pelayanan Kebersihan Lingkungan</title>
  </head>

  <body>
    <div class="brand text-center">
      <p>Go <span>Sari</span></p>
      <hr>
    </div>
    <div class="title text-center mb-5">
      <h3>Pembayaran</h3>
      <p>Silakan ikuti petunjuk di bawah ini</p>
    </div>
    <div class="container">
      <div class="row pb-5">
        <div class="col-md-5 col-sm-12 mx-auto">
          <div class="">
            <p class="text-white">Rincian Pembayaran</p>
            <div class="card card-body mb-3" style="border-radius: 5px;">
              <p class="text-center my-auto" style="font-weight: 600;">Total Tagihan: Rp. {{ number_format($invoice->total_amount) }}</p>
            </div>
            <div class="card" style="border-radius: 5px;">
              <div class="card-body">
                <p>Silakan Transfer ke nomor rekening di bawah ini.</p>
                @foreach($banks as $bank)
                <div class="rekening mb-2">
                  <strong>{{ $bank->name }}</strong> : <span>{{ $bank->number }}</span>
                  <br><small>A/N. {{ $bank->owner }}</small>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5 col-sm-12 mx-auto">
          <div class="form-payment">
            <form action="{{ url('payment/upload-receipt') }}" method="POST" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="code" value="{{ $invoice->code }}">
              <div class="mb-3 mt-3">
                <label for="name" class="form-label text-white">Nama Pengirim</label>
                <input readonly type="text" class="form-control" id="name" placeholder="{{ $invoice->user->name }}">
              </div>
              <div class="mb-3">
                <label for="email" class="form-label text-white">Email Pengirim</label>
                <input readonly type="email" class="form-control"placeholder="{{ $invoice->user->email }}" id="email">
              </div>
              <div class="mb-3">
                <label for="payment_receipt" class="form-label text-white">Upload Bukti Transfer </label>
                <input name="payment_receipt" type="file" class="form-control"
                  id="payment_receipt" placeholder="Upload Bukti Transfer" required/>
              </div>
              <div class="row">
                <div class="col-md-6 mt-2">
                  <button type="submit" class="btn btn-primary btn-pay">Bayar Tagihan</button>
                </div>
                <div class="col-md-6 mt-2">
                  <a href="{{ url()->previous() }}" class="btn btn-light btn-cancel">Kembali</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>         
    </div>
  </body>

</html>