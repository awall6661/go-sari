<!doctype html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <link rel="shortcut icon" href="{{ asset('img/favicon-icon.png') }}">
        <!-- fontawesome -->
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

       <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      

        <!-- My style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/landing-page/main.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/landing-page/landing-style.css') }}">
        
        <title>Go Sari - Unit Pelayanan Kebersihan Lingkungan</title>
    </head>
  <body>
<section class="landingPage">
    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top" data-aos="fade-right"">
      <div class="container">
        <a class="navbar-brand" href="{{ route('landing') }}">
          <img src="{{asset('img/landing-page/brand.png')}}" class="img-fluid img-brand" alt="logo"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav mr-auto">
            <a class="nav-item nav-link active" href="{{ route('landing') }}">Home</a>
            <a class="nav-item nav-link" href="https://guwosarimaju.id/about-us/">About</a>
            <a class="nav-item nav-link" href="https://guwosarimaju.id/">Guwosari</a>
            <a class="nav-item nav-link" href="https://api.whatsapp.com/send?phone=6289625637988">Contact</a>
          </div>
          <div class="navbar-nav ml-auto">
            @if (Route::has('login'))
              @auth
<<<<<<< HEAD
              <a class="btn btn-login mr-2" href="{{ route('dashboard') }}">Dashboard</a>
=======
>>>>>>> 511b5057757a65c88853e0827b76106a89377b3f
              @else
                <a class="btn btn-login mr-2" href="{{ route('login') }}">Masuk</a>
                @if (Route::has('register'))
                <a class="btn btn-register" href="{{ route('register') }}">Daftar</a>
                @endif
              @endauth
            @endif
          </div>
        </div>
      </div>
    </nav>

    <!-- Jumbotron -->
    <div class="jumbotron bg-white">
        <div class="container">
            <div class="row justify-content-between align-items-center ">
                <div class="col-md-12 col-lg-5 col-sm-12 text-md-left mb-5 content" data-aos="fade-right">
                    
                        <div class="text-head">
                            <p>Unit Pelayanan Kebersihan Lingkungan</p>
                        
                        </div>
                    <h1 class="h1 font-weight-bold title">Unit <span class="title-color">Go Sari</span> Bumdes
                        Guwosari Maju</h1>
                    <p class=" text-secondary ">
                        Unit Pelayanan Kebersihan Lingkungan (Go-Sari) merupakan layanan
                        pengelolaan sampah rumah tangga dan sejenisnya yang dilakukan bersama-sama
                        dengan warga Kalurahan Guwosari dan sekitarnya
                    </p>

                    <a class="btn btn-primary btn-cta" href="{{ route('register') }}" role="button">Daftar
                        Sekarang</a>
                </div>
                <div class="col-md-6 col-lg-5 col-sm-12">
                    <img src="{{ asset('img/landing-page/vector 1.png') }}" class="img-fluid element1" alt=""
                        data-aos="fade-left" />
                    <div class="jumbotron-img">
                        <img src="{{ asset('img/landing-page/tree.png') }}" class="img-fluid tree" alt="" />
                        <img src="{{ asset('img/landing-page/tree.png') }}" class="img-fluid tree2" alt="" />
                        <img src="{{ asset('img/landing-page/Group 1422.png') }}" class="img-fluid element2"
                            alt="" />

                        <img src="{{ asset('img/landing-page/cloud 1.png') }}" class="img-fluid element4"
                            alt="" />
                        <img src="{{ asset('img/landing-page/cloud 2.png') }}" class="img-fluid element5"
                            alt="" />
                        <img src="{{ asset('img/landing-page/cloud 2.png') }}" class="img-fluid element6"
                            alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron ends -->
    
    <!-- Step start -->
    <div class="step">
        <div class="container">
        <div class="d-flex justify-content-center sub-title">
        <p data-aos="fade-up">Proses</p>
        </div>
            <div class="title" data-aos="fade-down">
                <p class="text-center">Proses <span class="title-color">Pengambilan</span>
                </p>
                <p class="text-center">hingga <span class="title-color">Penyaluran</span> Sampah</p>
                <span class="section-line"></span>
            </div>



        <div class="process" data-aos="fade-up">
            <!-- <img src="{{ asset('img/landing-page/element.png') }}" class="element2" data-aos="fade-up" alt=""> -->
            <img src="{{ asset('img/landing-page/element.png') }}" class="element" alt="">
                <div class="row">

                    <div class="col-md-12 col-lg-4  col-sm-12 text-center">
                        <div class="card process-card">
                            <div class="process-card-image text-center">
                                <img src="{{ asset('img/landing-page/process 1.png') }}" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Pemilihan Sampah</h5>
                                <p class="card-text">
                                    Pemilihan Sampah Oleh Nasabah
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  col-lg-4  col-sm-12 text-center">
                        <div class="card process-card">
                            <div class="process-card-image text-center">
                                <img src="{{ asset('img/landing-page/process 2.png') }}" alt=""
                                    class="img-process-2">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Penjemputan Sampah</h5>
                                <p class="card-text">
                                    Penjemputan Sampah Oleh Petugas Sampah untuk di kumpulkan di tempat
                                    pengolahan sampah
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12  col-lg-4 col-sm-12 text-center">
                        <div class="card process-card text-center">
                            <div class="process-card-image">
                                <img src="{{ asset('img/landing-page/process 3.png') }}" alt="">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Pengolahan Sampah</h5>
                                <p class="card-text">
                                    Pengolahan oleh petugas pengolahan
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Step end -->

    <!-- Bnefit Start -->
    <div class="benefit">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-12 col-sm-12 col-lg-5" data-aos="fade-right">
                    <div class="feature-detail">
                    <div class="d-flex justify-content-start sub-title">
                        <p data-aos="fade-up">Manfaat</p>
                    </div>
                        <p class="h3 font-weight-bold title">Unit <span class="title-color">Go Sari</span> Bumdes
                        </p>
                        <p class="h3 font-weight-bold title">Guwosari Maju</p>
                        <span class="feature-line"></span>
                        <div class="detail-benefit">
                            <div class="row">
                                <div class="col-2">
                                    <img src="{{ asset('img/landing-page/rycycle.png') }}" alt="">
                                </div>
                                <div class="col-10">
                                    <h5 class="font-weight-bold">Proses Daur Ulang Lebih Efektif & Efisein
                                    </h5>
                                    <p class="text-secondary text-justify">
                                        Saat ini kami sudah melayani lebih dr 150 nasabah yg akan terus
                                        bertambah,

                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <img src="{{ asset('img/landing-page/heathy.png') }}" alt="">
                                </div>
                                <div class="col-10">
                                    <h5 class="font-weight-bold">Mengubah lingkungan menjadi lebih sehat
                                    </h5>
                                    <p class="text-secondary text-justify">
                                        Saat ini kami sudah melayani lebih dr 150 nasabah yg akan terus
                                        bertambah,

                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                    <img src="{{ asset('img/landing-page/money.png') }}" alt="">
                                </div>
                                <div class="col-10">
                                    <h5 class="font-weight-bold">Mendapat Keuntungan Ekonomi </h5>
                                    <p class="text-secondary text-justify">
                                        Saat ini kami sudah melayani lebih dr 150 nasabah yg akan terus
                                        bertambah,
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-5 img" data-aos="fade-right">
                    <img src="{{ asset('img/landing-page/benefit-img-1.png') }}" class="img-fluid element1"
                        alt="" />
                    <img src="{{ asset('img/landing-page/benefit-img-2.png') }}" class="img-fluid element2"
                        alt="" />
                    <img src="{{ asset('img/landing-page/benefit-img-3.png') }}" class="img-fluid element3"
                        alt="" />
                    <img src="{{ asset('img/landing-page/benefit.png') }}" class="img-benefit" alt=""
                        data-aos="fade-right" />

                </div>
            </div>
        </div>

    </div>
    <!-- Benefit End -->
    
    <!-- Footer Start -->
    <footer>
        <div class="container">
            <div class="row ">
                <div class="col-md-12 col-sm-12 col-lg-4">
                    <!-- footer about -->
                    <div class="footer-about">
                        <div class="footer-about__img mb-5">
                            <img src="{{ asset('img/landing-page/logo.png') }}"
                                class="img-fluid footer-logo" alt="" />
                        </div>
                        <p>
                            Badan Usaha Milik Desa Guwosari Maju, berdiri sejak tahun 2016 (Perdes No.5
                            Tahun 2016)
                        </p>
                        <p>©Bumdes Guwosari Maju 2021. All rights reserved</p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-3 account">
                    <p class="footer-title">Rekening</p>
                    <ul>
                        <li>
                            <p>A/n Bumdes Guwosari Maju</p>
                            </liz>
                        <li>
                            <img src="{{ asset('img/landing-page/BRI.png') }}" alt=""> : 1117991898
                        </li>
                        <li>
                            <img src="{{ asset('img/landing-page/BNI.png') }}" alt=""> : 6635-01-018610-53-5
                        </li>
                        <li>
                            <p>NPWP : 84.439.984.0-543.000
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-2">
                    <p class="footer-title">Social Media</p>
                    <div class="social-media">
                        <ul>
                            <li><a href="https://www.instagram.com/bumdesguwosari/"><i
                                        class="fab fa-instagram"></i></a><a
                                    href="https://web.facebook.com/Bumdes-Guwosari-Maju-258154138788074?_rdc=1&_rdr"><i
                                        class="fab fa-facebook ms-4"></i></a></li>
                            <li></li>
                            <li><a href="https://twitter.com/bumdesguwosari"><i
                                        class="fab fa-twitter"></i></a><a
                                    href="https://www.youtube.com/channel/UCMNgJ47OYRdqXK3vPKnLhvA?view_as=subscriber"><i
                                        class="fab fa-youtube ms-4"></i></a></li>
                            <li></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-lg-2">
                    <p class="footer-title">Address</p>
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1011752.4030018495!2d110.314286!3d-7.87855!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbcb118c4959c7459!2sBumdes%20Guwosari%20Maju!5e0!3m2!1sid!2sid!4v1623407292262!5m2!1sid!2sid"
                        width="300" height="250" style="border:0;" allowfullscreen=""
                        loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer ends -->
</section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

        <script src="{{ asset('js/landing-page/index.js') }}"></script>

        <script src="https://kit.fontawesome.com/abae7fb329.js" crossorigin="anonymous"></script>
  </body>
<<<<<<< HEAD
</html>
=======
</html>
>>>>>>> 511b5057757a65c88853e0827b76106a89377b3f
