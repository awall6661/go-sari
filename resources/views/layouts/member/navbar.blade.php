<section class="ladingPage">
  <nav class="navbar navbar-expand-lg navbar-light custom-navbar fixed-top" data-aos="fade-right">
  <div class="container">
        <a class="navbar-brand" href="{{ route('landing') }}">
          <img src="{{asset('img/landing-page/brand.png')}}" class="img-fluid img-brand" alt="logo"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div class="navbar-nav mr-auto">
            <a class="nav-item nav-link active" href="{{ route('landing') }}">Home</a>
            <a class="nav-item nav-link" href="https://guwosarimaju.id/about-us/">About</a>
            <a class="nav-item nav-link" href="https://guwosarimaju.id/">Guwosari</a>
            <a class="nav-item nav-link" href="https://api.whatsapp.com/send?phone=6289625637988">Contact</a>
          </div>
          <div class="navbar-nav ml-auto">
            @if (Route::has('login'))
              @auth
              @else
                <a class="btn btn-login mr-2" href="{{ route('login') }}">Masuk</a>
                @if (Route::has('register'))
                <a class="btn btn-register" href="{{ route('register') }}">Daftar</a>
                @endif
              @endauth
            @endif
          </div>
        </div>
      </div>
  </nav>
</section>