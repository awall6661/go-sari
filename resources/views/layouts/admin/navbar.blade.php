<?php
  $admin = auth()->user();
  $notifications = $admin->unreadNotifications;
?>

<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
  <form class="form-inline mr-auto">
    <ul class="navbar-nav mr-3">
      <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
  </form>
  <ul class="navbar-nav navbar-right">
    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" id="notificationToggle" class="nav-link notification-toggle nav-link-lg"><i class="far fa-bell"></i></a>
      <div class="dropdown-menu dropdown-list dropdown-menu-right">
        <div class="dropdown-header">Notifikasi
          <div class="float-right">
            <a id="markAllRead" href="javascript:void(0)" style="display:none">Tandai Telah Dibaca</a>
          </div>
        </div>
        <div class="dropdown-list-content dropdown-list-icons" id="notificationDropdown">
          @forelse($notifications as $notification)
            <a href="{{ $notification->data['url'] }}" class="dropdown-item dropdown-item-unread" data-id="{{ $notification->id }}">
              <div class="dropdown-item-icon bg-primary text-white">
                <i class="fas fa-exclamation"></i>
              </div>
              <div class="dropdown-item-desc">
                <strong>[ {{ ucfirst($notification->data['type']) }} ]</strong>
                {{ $notification->data['name'] }} ({{ $notification->data['email'] }}) {{$notification->data['message']}}
                <div class="time text-primary">
                  {{date('d/m/Y H:i', strtotime($notification->created_at))}}                  
                </div>
                
              </div>
            </a>             
            @empty
            <span class="ml-3">
              Tidak ada notifikasi.
            </span>
          @endforelse  
        </div>
      </div>
    </li>
    
    <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        <img alt="image" src="{{ asset('img/admin/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
        <div class="d-sm-none d-lg-inline-block">{{ Auth::guard('admin')->user()->name }}</div>
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <a href="{{ route('admin.logout') }}" class="dropdown-item has-icon text-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class=" fas fa-sign-out-alt"></i> Logout
        </a>
        <form action="{{ route('admin.logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
      </div>
    </li>
  </ul>
</nav>
