@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Daftar Pelanggan</h1>
      <div class="section-header-breadcrumb">
        {{-- <div class="breadcrumb-item active"><a href="#">Dashboard</a></div> --}}
        <div class="breadcrumb-item"><a href="#">Member</a></div>
        <div class="breadcrumb-item">Akun</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-sm-12 col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>Daftar Pelanggan</h4>
            </div>
            <div class="card-body">
              <ul class="nav nav-tabs" id="memberTab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all"
                    aria-selected="true">Semua ({{ $members->count()}})</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="active-member-tab" data-toggle="tab" href="#active-member" role="tab"
                    aria-controls="active-member" aria-selected="false">Aktif
                    ({{ $activeMembers->count() }})</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="inactive-member-tab" data-toggle="tab" href="#inactive-member" role="tab"
                    aria-controls="inactive-member" aria-selected="false">Tidak Aktif
                    ({{ $inactiveMembers->count() }})</a>
                </li>
              </ul>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                  <div class="card">
                    <div class="card-body">
                      <div class="table-responsive">
                        <table id="memberTable" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Status</th>
                              <th>Invoice</th>
                              <th>Nama</th>
                              <th>NIK</th>
                              <th>No. HP</th>
                              <th>Alamat</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($members as $member)
                            <tr>
                              <td>{{ $member->id }}</td>
                              <td>
                                @if($member->status == 0)
                                <button class="btn btn-round btn-sm btn-danger change-status px-2"
                                  data-id="{{ $member->id }}" data-status="1" data-toggle="tooltip"
                                  title="Klik untuk aktifkan akun">Inactive
                                </button>
                                @else
                                <button class="btn btn-round btn-sm btn-success change-status px-2"
                                  data-id="{{ $member->id }}" data-status="0" data-toggle="tooltip"
                                  title="Klik untuk non-aktifkan akun">Active
                                </button>
                                @endif
                              </td>
                              <td>
                                <?php
                                    $currentMonth = date('Y-m');
                                    $currentInvoice = \App\Models\Invoice::where('user_id', $member->id)
                                        ->where('payment_due', 'like', '%' . $currentMonth . '%')->first();
                                ?>
                                <a href="{{ route('members.create_invoice', $member->id) }}"
                                  class="btn btn-sm btn-success mr-1" data-toggle="tooltip" title="Buat Invoice"><i
                                    class="fas fa-plus-circle"></i></a>
                                @if(!empty($currentInvoice))
                                <a href="{{ route('invoices.show', $currentInvoice->code) }}"
                                  class="btn btn-sm btn-info mr-1" data-toggle="tooltip"
                                  title="Detail invoice bulan ini"><i class="fas fa-info-circle"></i></a>
                                @endif
                              </td>
                              <td>{{ ucfirst($member->name) }}</td>
                              <td>{{ $member->nik }}</td>
                              <td>{{ $member->phone }}</td>
                              <td>{{ $member->address }}</td>
                              <td>
                                <a href="{{ url('admin/members/'.$member->id.'/details') }}"
                                  class="btn btn-sm btn-info mr-1" data-toggle="tooltip" title="Detail"><i
                                    class="fas fa-info-circle"></i></a>
                                <a href="#" class="btn btn-sm btn-primary mr-1" data-toggle="tooltip" title="Edit"><i
                                    class="fas fa-pencil-alt"></i></a>
                                <button class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip" title="Hapus"
                                  data-id="{{ $member->id }}">
                                  <i class="fas fa-trash"></i></button>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="active-member" role="tabpanel" aria-labelledby="active-member-tab">
                  <div class="card">
                    <div class="card-body">
                      <div class="table-responsive">
                        <table id="activeMemberTable" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Status</th>
                              <th>Invoice</th>
                              <th>Nama</th>
                              <th>NIK</th>
                              <th>No. HP</th>
                              <th>Alamat</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($activeMembers as $member)
                            <tr>
                              <td>{{ $member->id }}</td>
                              <td>
                                @if($member->status == 0)
                                <button class="btn btn-round btn-sm btn-danger change-status px-2"
                                  data-id="{{ $member->id }}" data-status="1" data-toggle="tooltip"
                                  title="Klik untuk aktifkan akun">Inactive
                                </button>
                                @else
                                <button class="btn btn-round btn-sm btn-success change-status px-2"
                                  data-id="{{ $member->id }}" data-status="0" data-toggle="tooltip"
                                  title="Klik untuk non-aktifkan akun">Active
                                </button>
                                @endif
                              </td>
                              <td>
                                <?php
                                        $currentMonth = date('Y-m');
                                        $currentInvoice = \App\Models\Invoice::where('user_id', $member->id)
                                            ->where('payment_due', 'like', '%' . $currentMonth . '%')->first();
                                    ?>
                                <a href="{{ route('members.create_invoice', $member->id) }}"
                                  class="btn btn-sm btn-success mr-1" data-toggle="tooltip" title="Buat Invoice"><i
                                    class="fas fa-plus-circle"></i></a>
                                @if(!empty($currentInvoice))
                                <a href="{{ route('invoices.show', $currentInvoice->code) }}"
                                  class="btn btn-sm btn-info mr-1" data-toggle="tooltip"
                                  title="Detail invoice bulan ini"><i class="fas fa-info-circle"></i></a>
                                @endif
                              </td>
                              <td>{{ ucfirst($member->name) }}</td>
                              <td>{{ $member->nik }}</td>
                              <td>{{ $member->phone }}</td>
                              <td>{{ $member->address }}</td>
                              <td>
                                <a href="#" class="btn btn-sm btn-info mr-1" data-toggle="tooltip" title="Detail"><i
                                    class="fas fa-info-circle"></i></a>
                                <a href="#" class="btn btn-sm btn-primary mr-1" data-toggle="tooltip" title="Edit"><i
                                    class="fas fa-pencil-alt"></i></a>
                                <button class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip" title="Hapus"
                                  data-id="{{ $member->id }}">
                                  <i class="fas fa-trash"></i></button>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="inactive-member" role="tabpanel" aria-labelledby="inactive-member-tab">
                  <div class="card">
                    <div class="card-body">
                      <div class="table-responsive">
                        <table id="inactiveMemberTable" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Status</th>
                              <th>Invoice</th>
                              <th>Nama</th>
                              <th>NIK</th>
                              <th>No. HP</th>
                              <th>Alamat</th>
                              <th>Aksi</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($inactiveMembers as $member)
                            <tr>
                              <td>{{ $member->id }}</td>
                              <td>
                                @if($member->status == 0)
                                <button class="btn btn-round btn-sm btn-danger change-status px-2"
                                  data-id="{{ $member->id }}" data-status="1" data-toggle="tooltip"
                                  title="Klik untuk aktifkan akun">Inactive
                                </button>
                                @else
                                <button class="btn btn-round btn-sm btn-success change-status px-2"
                                  data-id="{{ $member->id }}" data-status="0" data-toggle="tooltip"
                                  title="Klik untuk non-aktifkan akun">Active
                                </button>
                                @endif
                              </td>
                              <td>
                                <?php
                                        $currentMonth = date('Y-m');
                                        $currentInvoice = \App\Models\Invoice::where('user_id', $member->id)
                                            ->where('payment_due', 'like', '%' . $currentMonth . '%')->first();
                                    ?>
                                <a href="{{ route('members.create_invoice', $member->id) }}"
                                  class="btn btn-sm btn-success mr-1" data-toggle="tooltip" title="Buat Invoice"><i
                                    class="fas fa-plus-circle"></i></a>
                                @if(!empty($currentInvoice))
                                <a href="{{ route('invoices.show', $currentInvoice->code) }}"
                                  class="btn btn-sm btn-info mr-1" data-toggle="tooltip"
                                  title="Detail invoice bulan ini"><i class="fas fa-info-circle"></i></a>
                                @endif
                              </td>
                              <td>{{ ucfirst($member->name) }}</td>
                              <td>{{ $member->nik }}</td>
                              <td>{{ $member->phone }}</td>
                              <td>{{ $member->address }}</td>
                              <td>
                                <a href="#" class="btn btn-sm btn-info mr-1" data-toggle="tooltip" title="Detail"><i
                                    class="fas fa-info-circle"></i></a>
                                <a href="#" class="btn btn-sm btn-primary mr-1" data-toggle="tooltip" title="Edit"><i
                                    class="fas fa-pencil-alt"></i></a>
                                <button class="btn btn-sm btn-danger btn-delete" data-toggle="tooltip" title="Hapus"
                                  data-id="{{ $member->id }}">
                                  <i class="fas fa-trash"></i></button>
                              </td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
</section>


@endsection
@section('script')

@if (Session::has('success'))
<script>
iziToast.success({
  position: 'topRight',
  title: 'Sukses',
  message: '{{ Session::get("success") }}',
  timeout: 3000
});
</script>
@endif
<script>
$(document).ready(function() {
  $('#memberTable').DataTable();
  $('#activeMemberTable').DataTable();
  $('#inactiveMemberTable').DataTable();

  $('.change-status').click(function() {
    var member_id = $(this).data('id');
    var status = $(this).data('status');

    swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan mengubah status akun pelanggan.',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "post",
            dataType: "json",
            url: '/admin/members/change-status',
            data: {
              'status': status,
              'member_id': member_id
            },
            success: function(data) {
              window.location.reload();
            }
          });
          swal('Status akun pelanggan berhasil dirubah.', {
            icon: 'success',
          });
        } else {
          swal('Status pelanggan tidak dirubah.');
        }
      });
  });

  $('.btn-delete').click(function() {
    var member_id = $(this).data('id');

    swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan menghapus akun pelanggan.',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "delete",
            dataType: "json",
            url: '/admin/members/delete/' + member_id,
            data: {
              'member_id': member_id
            },
            success: function(data) {
              window.location.reload();
            }
          });
          swal('Akun pelanggan berhasil dihapus.', {
            icon: 'success',
          });
        } else {
          swal('Akun pelanggan tidak dihapus.');
        }
      });
  });
});
</script>
</div>
@endsection
