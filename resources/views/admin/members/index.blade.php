@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Pelanggan</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Pelanggan</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-sm-12 col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>Data Pelanggan</h4>
            </div>
            <div class="card-body">
              <div class="mb-3">
                <ul class="nav nav-tabs" id="myTab1" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active status-tab" data-status="all" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="true">Semua (<span id="countAll"></span>)</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link status-tab" data-status="active" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="false">Aktif (<span id="countActive"></span>)</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link status-tab" data-status="inactive" data-toggle="tab" href="javascript:void(0)" role="tab" aria-selected="false">Tidak Aktif (<span id="countInactive"></span>)</a>
                  </li>
                </ul>
              </div>
              <div class="table-responsive">
                <table id="memberTable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Status</th>
                      <th>Nama</th>
                      <th>NIK</th>
                      <th>No. HP</th>
                      <th>Alamat</th>
                      <th>Lok. Sampah</th>
                      <th>Tempat Sampah</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')

@if (Session::has('success'))
<script>
iziToast.success({
  position: 'topRight',
  title: 'Sukses',
  message: '{{ Session::get("success") }}',
  timeout: 3000
});
</script>
@endif
<script>
  $(document).ready(function() {
    function getCountData() {
      $.get( "/admin/members/count", function( data ) {
        $('#countAll').text(data.all);
        $('#countActive').text(data.active);
        $('#countInactive').text(data.inactive);
      });
    }

    function changeMemberStatus(id, memberStatus, filterStatus) {
      swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan mengubah status akun pelanggan.',
        icon: 'warning',
        buttons: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "post",
            dataType: "json",
            url: '/admin/members/change-status',
            data: {
              '_token': "{{ csrf_token() }}",
              'status': memberStatus,
              'id': id
            },
            success: function(data) {
              table.ajax.url("/admin/members/data?status="+filterStatus).load();

              getCountData();
            }
          });
          swal('Status akun pelanggan berhasil dirubah.', {
            icon: 'success',
          });
        } else {
          swal('Status pelanggan tidak dirubah.');
        }
      });
    }

    getCountData();

    let table = $('#memberTable').DataTable({
      "ajax": "/admin/members/data?status=all",
      "columns":[
        {"data": "id"},
        {
          "data": null,
          "render" : function (data, type, row) {
              if (data.status == '0') {
                  return '<button class="btn btn-round btn-sm btn-danger change-status px-2" data-toggle="tooltip" data-status="1" data-id="'+data.id+'" title="Klik untuk aktifkan akun">Tdk Aktif</button>';
              } else {
                return '<button class="btn btn-round btn-sm btn-success change-status px-2" data-toggle="tooltip" data-status="0" data-id="'+data.id+'" title="Klik untuk non-aktifkan akun">Aktif</button>';
              }
          }
        },
        {"data": "name"},
        {"data": "nik"},
        {"data": "phone"},
        {"data": "address"},
        {"data": "garbage_can_location"},
        {
          "data": "garbage_can_picture",
          "render": function (data) {
                return '<a href="/img/member/garbage/'+data+'" target="_blank" class="btn btn-sm btn-info" data-toggle="tooltip">Lihat</a>';
          }
        },
        {"data": "id",
          "render": function (data) {
                return '<a href="/admin/members/detail/'+data+'" class="btn btn-sm btn-info mr-1" data-toggle="tooltip" title="Detail"><i class="fas fa-info-circle"></i></a>';
          }
        }
      ],
    });
      // Add increment no. in first column
    table.on( 'draw.dt', function () {
      let PageInfo = $('#memberTable').DataTable().page.info();
        table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
          cell.innerHTML = i + 1 + PageInfo.start;
      });
    });

    // Change member status
    $('#memberTable tbody').on( 'click', '.change-status', function () {
      let id = $(this).data('id');
      let memberStatus = $(this).data('status');
      let filterStatus = $( "a.status-tab[aria-selected='true']" ).data('status');

      changeMemberStatus(id, memberStatus, filterStatus);
    });

    $('.status-tab').click(function() {
      let status = $(this).data('status');
      table.ajax.url("/admin/members/data?status="+status).load();
    });
  });
</script>
</div>
@endsection
