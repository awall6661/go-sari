@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Transaksi</h1>
    </div>
    <div class="section-body">
      <div class="card">
        
        <div class="card-body">

          <div class="row">
            <div class="col-lg-12">
              <div class="invoice-title">
                <h3>Tagihan Pembayaran</h3>
                <div class="invoice-number">Invoice Code : {{ $invoice->code }}</div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <address>
                    <strong>Tagihan untuk :</strong><br>
                    {{ ucfirst($invoice->user->name) }}<br>
                    Dusun {{ $invoice->user->hamlet->hamlet_name }}<br>
                    RT {{ $invoice->user->rt }} <br>
                    {{ $invoice->user->address }} <br>
                  </address>
                </div>
                <div class="col-md-6 text-md-right">
                  <address>
                    <strong>Dari :</strong><br>
                    Go Sari
                  </address>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <address>
                    <strong>Status Pembayaran : </strong><br>
                    @if($invoice->payment_status == "paid")
                    <p>Lunas</p>
                    @elseif($invoice->payment_status == "pending")
                    <p>Menunggu Konfirmasi</p>
                    @else
                    <p>Belum Lunas</p>
                    @endif
                  </address>
                </div>
                <div class="col-md-6 text-md-right">
                  <address>
                    <strong>Kategori Sampah :</strong><br>
                    {{ $invoice->user->garbageCategory->category_name }}<br>
                  </address>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <address>
                    <strong>Metode Pembayaran :</strong><br>
                    {{ $invoice->payment_status == "unpaid" ? '-' : 'Transfer Bank' }}<br><br>
                  </address>
                </div>
                <div class="col-md-6 text-md-right">
                  <address>
                    <strong>Tanggal Pembayaran : </strong><br>
                    {{ empty($invoice->payment_date) ? '-' : $invoice->payment_date }}<br><br>
                  </address>
                </div>
              </div>
            </div>
          </div>

          <div class="row mt-4">
            <div class="col-md-12">
              <div class="section-title">Ringkasan Tagihan</div>
              <div class="table-responsive">
                <table class="table table-striped table-hover table-md">
                  <tr>
                    <th>#ID</th>
                    <th class="text-center">Kode Tagihan</th>
                    <th class="text-center">Bulan</th>
                    <th class="text-center">Batas Pembayaran</th>
                    <th class="text-right">Total Tagihan</th>
                  </tr>
                  <tr>
                    <td>{{ $invoice->id }}</td>
                    <td class="text-center">{{ $invoice->code }}</td>
                    <td class="text-center">{{ $invoice->month }}</td>
                    <td class="text-center">{{ date('d M Y', strtotime($invoice->payment_due)) }}
                    </td>
                    <td class="text-right">Rp. {{ number_format($invoice->total_amount) }}</td>
                  </tr>
                </table>
              </div>
              <div class="row mt-4">
                <div class="col-lg-8">
                  <div>
                    
                    <a href="{{ route('invoices.download', $invoice->code) }}"><button class="btn btn-warning btn-icon icon-left"><i class="fas fa-print"></i> Download</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection