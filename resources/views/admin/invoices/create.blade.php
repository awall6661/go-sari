@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Invoice</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ url('admin/invoices') }}">Invoice</a></div>
        <div class="breadcrumb-item">Buat Invoice</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <form method="post" action="{{ route('invoices.store') }}" class="needs-validation" novalidate="">@csrf
              <div class="card-header">
                <h4>Buat Invoice</h4>
              </div>
              <div class="card-body">
              <div class="form-group">
                <label>Bulan</label>
                <select class="form-control" name="month" id="month" required="">
                  <option selected value="" disabled>Pilih Bulan...</option>
                  @foreach ($months as $m)
                  <option value="{{ $m }}">{{ $m }}</option>
                  @endforeach
                </select>
                <div class="invalid-feedback">
                  Isian masih kosong
                </div>
              </div>
              <div class="form-group">
                <label>Tahun</label>
                <select class="form-control" name="year" id="year" required="">
                  <option selected value="" disabled>Pilih Tahun...</option>
                  @foreach ($years as $y)
                  <option value="{{ $y }}">{{ $y}}</option>
                  @endforeach
                </select>
                <div class="invalid-feedback">
                  Isian masih kosong
                </div>
              </div>
              <div class="form-group">
                <label for="">Pelanggan</label>
                <select class="form-control" name="members" id="members" required="">
                  <!-- <option value="0" selected disabled>Pilih Pelanggan...</option> -->
                  <option value="all">Semua Pelanggan</option>
                </select>
              </div>
              <div class="form-group">
                <label>Batas Pembayaran</label>
                <input type="datetime-local" class="form-control" id="dueTo" name="payment_due" required="" />
                <div class="invalid-feedback">
                  Isian masih kosong
                </div>
              </div>

          </div>
          <div class="card-footer text-right">
            <button class="btn btn-primary">Submit</button>
          </div>
          </form>
        </div>
      </div>
    </div>
</div>
</section>
</div>
@endsection
@section('script')
<script>
// $.validate({
//   form: '#validation_style',
//   modules: 'security'
// });

// $(document).ready(function () {
//   $('#month').on('change',function() {
//     // var year = $("#year").val();
//     var month = $(this).val();
//     // alert(month + " " + year);
//     $.ajax({
//       url:"/admin/invoices/users",
//       type:"POST",
//       data: {
//       "month": month,
//       },
//       success:function (data) {
//         console.log(data);
//           $("#user").empty();
//           $("#user").append("<option value='a'>Pilih Pelanggan...</option>");
//           for( var i = 0; i<data.length; i++){
//               var email = data[i]['email'];
//               var name = data[i]['name'];
//               var amount = data[i]['total_amount'];
              
//               // console.log(amount);
//               $("#user").append("<option value='"+email+"'>"+name+"</option>");
//           }
//       },
//       error:function(){
//         // 
//       }
//     });
//   });

//   $('#user').on('change',function() {
//     var email = $(this).val();
//     console.log(email);

//     $.ajax({
//       url:'/admin/invoices/users/details',
//       type:"POST",
//       data:{"user_email": email},
//       success:function(data) {
//         $("#amount").val(data.price);
//         $("#category").val(data.category_name);
//       },
//       error: function() {
//         // 
//       }
//     });
//   });
// });
</script>
@endsection
