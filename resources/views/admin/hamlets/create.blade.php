@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Dusun</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="{{ url('admin/hamlets') }}">Dusun</a></div>
        <div class="breadcrumb-item">Tambah Dusun</div>
      </div>
    </div>

    <div class="section-body">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-8">
          <div class="card">
            <form method="post" action="{{ route('hamlets.store') }}" class="needs-validation" novalidate="">
            @csrf
              <div class="card-header">
                <h4>Tambah Dusun</h4>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Dusun</label>
                  <input type="text" class="form-control" name="hamlet_name" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="form-group">
                  <label>Target Pelanggan</label>
                  <input type="number" class="form-control" name="target_qty" required="">
                  <div class="invalid-feedback">
                    Isian masih kosong
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
