@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Transaksi Pembayaran</h1>
      <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Transaksi Pembayaran</div>
      </div>
    </div>

    <div class="section-body">

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Data Pembayaran</h4>
            </div>
            <div class="card-body">
              <div class="row mb-1">
                <ul class="nav nav-pills px-3" id="myTab1" role="tablist">
                  @foreach($months as $m)
                  <li class="nav-item">
                    <a class="nav-link filterMonth" data-toggle="tab" data-month="{{ $m }}" href="javascript:void(0)" role="tab" aria-selected="false">{{ $m }}</a>
                  </li>
                  @endforeach
                </ul>
              </div>
              <a href="javascript:void(0)" class="btn btn-info mt-2" id="allMonths">Semua Bulan</a>
              <div class="mb-3 mt-2 text-right">
                  <a href="{{url('admin/report/export')}}" id="printBtn" class="btn btn-warning"><i class="fas fa-file-export"></i> Export Excel</a>
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-bordered" id="transactionTable">
                  <thead>
                    <tr>
                        <th class="text-center">No.</th>
                        <th class="text-center">Tanggal Transaksi</th>
                        <th class="text-center">Bulan</th>
                        <th class="text-center">Nama Pelanggan</th>
                        <th class="text-center">Dusun</th>
                        <th class="text-center">Kategori Sampah</th>
                        <th class="text-center">Nominal Tagihan</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                    <th colspan="6" class="text-right">Total</th>
                    <th></th>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
    let table = $('#transactionTable').DataTable({
      "ajax": "/admin/report-data?month="+"{{$currentMonth}}",
      "columns":[
        {"data": "id"},
        {"data": "payment_date"},
        {"data": "month"},
        {"data": "name"},
        {"data": "hamlet_name"},
        {"data": "category_name"},
        {"data": "total_amount",
        "render": function (amount) { return "Rp. " + new Intl.NumberFormat().format(amount);}
        },
      ],
      "footerCallback": function ( row, data, start, end, display ) {
            let api = this.api().ajax.json();
            if(api){
                var td = $('tfoot').find('th');
                if (data.length > 0) {
                    $('table tfoot').show();
                    td.eq(1).html('Rp. ' + new Intl.NumberFormat().format(data[0].total));
                } else {
                    // td.eq(1).html('Rp. ' + new Intl.NumberFormat().format(0));
                    $('table tfoot').hide();
                }
            }
        }
    });

    // increment number
    table.on( 'draw.dt', function () {
      let increment = $('#transactionTable').DataTable().page.info();
          table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + increment.start;
        });
    });

    // filter month
    $('.filterMonth').click(function() {
        let month = $(this).data('month');
        table.ajax.url("/admin/report-data?month=" + month).load();
    });

    // reset the filter
    $('#allMonths').click(function() {
      $('.filterMonth').removeClass('active');
      table.ajax.url("/admin/report-data?month=all").load();
    });

    // add active class to month filter
    let thisMonth = "{{ $currentMonth }}"
    let filterMonthLink = $('a.filterMonth:contains("'+thisMonth+'")');
    filterMonthLink.attr('aria-selected', true);
    filterMonthLink.addClass('active');
  });
</script>
@endsection
