@extends('layouts.admin.main')
@section('content')
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Kelola Admin</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></div>
        <div class="breadcrumb-item">Kelola Admin</div>
      </div>
    </div>

    <div class="section-body">
        <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Data Admin</h4>
            </div>
            <div class="card-body">
              <div>
                <a href="{{ url('super-admin/admins/create') }}" class="btn btn-sm btn-primary mr-1 mb-3"><i
                    class="fas fa-plus"></i> Tambah Admin</a>
              </div>
              <div class="table-responsive">
                <table class="table table-striped table-bordered" id="category">
                  <thead>
                    <tr>
                      <th class="text-center">No.</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>No. HP</th>
                      <th>Role</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($admins as $admin)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$admin->name}}</td>
                      <td>{{$admin->email}}</td>
                      <td>{{$admin->phone}}</td>
                      <td>{{$admin->role}}</td>
                      <td>
                        <button class="btn btn-sm btn-danger mr-1 delete" data-id="{{$admin->id}}" data-toggle="tooltip" title="Hapus"><i class="fas fa-trash"></i></button>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@endsection
@section('script')
@if (Session::has('success'))
<script>
iziToast.success({
  position: 'topRight',
  title: 'Sukses',
  message: '{{ Session::get("success") }}',
  timeout: 3000
});
</script>
@endif

<script>
$(document).ready(function() {
  function deleteInvoice(id) {
      swal({
        title: 'Apakah Anda yakin?',
        text: 'Anda akan menghapus akun.',
        icon: 'warning',
        dangerMode:true,
        buttons: true,
      })
      .then((willChange) => {
        if (willChange) {
          $.ajax({
            type: "post",
            dataType: "json",
            url: '/super-admin/admins/delete/'+id,
            data: {
              '_token': "{{ csrf_token() }}",
              'id':id
            },
            success: function(data) {
              window.location.reload();
            }
          });
          swal('Data berhasil dihapus.', {
            icon: 'success',
          });
        } else {
          swal('Data batal dihapus.');
        }
      });
    }

  
  $('#category').DataTable();

  $('.delete').click(function() {
    let id = $(this).data('id');
    deleteInvoice(id);
  });
});
</script>
@endsection
