<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page');
})->name('landing');

Auth::routes(['verify' => true]);

// Routes for the users/members are defined here
Route::middleware(['auth'])->group(function () {
    Route::namespace('Member')->group(function () {
        Route::get('dashboard', 'MemberController@dashboard')->name('dashboard'); //->middleware('verified');

        Route::get('invoices', 'MemberController@getInvoices');
        Route::get('invoices/{code}', 'MemberController@getInvoiceDetail')->name('invoices.detail');
        Route::get('invoices/generate/{code}', 'MemberController@getInvoiceGenerate')->name('invoices.generate');

        Route::get('payment/{code}', 'MemberController@showPaymentPage');
        Route::post('payment/upload-receipt', 'MemberController@uploadPaymentReceipt');

        Route::get('profile', 'ProfileController@index')->name('member.profile');
        Route::post('profile/update', 'ProfileController@updateProfile')->name('member.update');
        Route::post('profil/update-avatar', 'ProfileController@saveAvatar');

        Route::get('transactions', 'TransactionController@index');

        Route::post('notifications/mark-as-read', 'MemberController@markNotification');
    });
});

// Routes for the admin are defined here
Route::prefix('admin')->namespace('Admin')->group(function () {
    Route::match(['get', 'post'], 'login', 'AuthController@login')->middleware('guest');
    Route::middleware(['auth:admin'])->group(function () {
        Route::get('dashboard', 'AdminDashboardController@index');
        Route::get('logout', 'AuthController@logout')->name('admin.logout');
        Route::resource('garbage-categories', 'GarbageCategoryController');
        Route::resource('hamlets', 'HamletController');

        Route::get('members', 'AdminController@indexMembers')->name('members.index');
        Route::get('members/detail/{id}', 'AdminController@getMemberDetails')->name('members.detail');
        Route::get('members/data', 'AdminController@getMembersData');
        Route::post('members/change-status', 'AdminController@changeMemberStatus');
        Route::get('members/count', 'AdminController@countMembers');
        // Route::post('members/delete/{id}', 'AdminController@destroyMember');

        Route::post('invoices/delete/{id}', 'InvoiceController@destroy');
        Route::get('invoices/data', 'InvoiceController@getData');
        Route::get('invoices/count', 'InvoiceController@count');
        Route::get('invoices/download/{code}', 'InvoiceController@downloadInvoice')->name('invoices.download');
        Route::post('invoices/confirm/{id}', 'InvoiceController@confirmPaymentManual')->name('invoices.confirm');

        Route::resource('invoices', 'InvoiceController');

        Route::get('transactions', 'TransactionController@index')->name('transactions.index');
        Route::get('transactions-data', 'TransactionController@getData');
        Route::get('transactions/count', 'TransactionController@count');
        Route::post('transactions/change-status', 'TransactionController@changeStatus');

        Route::resource('target-members', 'TargetMemberController');

        Route::get('report', 'ReportController@index')->name('report.index');
        Route::get('report-data', 'ReportController@getData');
        Route::get('report/export', 'ReportController@exportReport');

        Route::get('setting/banks', 'SettingController@indexBank')->name('setting.banks.index');
        Route::get('setting/banks/create', 'SettingController@createBank')->name('setting.banks.create');
        Route::post('setting/banks/create', 'SettingController@storeBank')->name('setting.banks.store');
        Route::get('setting/banks/edit/{id}', 'SettingController@editBank')->name('setting.banks.edit');
        Route::post('setting/banks/update/{id}', 'SettingController@updateBank')->name('setting.banks.update');
        Route::post('setting/banks/delete/{id}', 'SettingController@destroyBank')->name('setting.banks.destroy');

        Route::post('notifications/mark-as-read', 'AdminController@markNotification');
    });
});

Route::middleware(['auth:admin', 'superadmin'])->namespace('Admin')->prefix('super-admin')->group(function () {
    Route::get('admins', 'SuperAdminController@indexAdmins')->name('admin.index');
    Route::get('admins/create', 'SuperAdminController@createAdmin')->name('admin.create');
    Route::post('admins', 'SuperAdminController@storeAdmin')->name('admin.store');
    Route::post('admins/delete/{id}', 'SuperAdminController@destroyAdmin')->name('admin.destroy');
});
