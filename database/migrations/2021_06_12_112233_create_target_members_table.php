<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hamlet_id');
            $table->integer('target_qty');
            $table->timestamps();

            $table->foreign('hamlet_id')->references('id')->on('hamlets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_members');
    }
}
