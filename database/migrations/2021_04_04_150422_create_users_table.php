<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('nik', 16);
            $table->string('phone', 15);
            $table->unsignedBigInteger('garbage_category_id');
            $table->string('garbage_can_location');
            $table->string('garbage_can_picture');
            $table->unsignedBigInteger('hamlet_id');
            $table->string('rt', 2);
            $table->text('address');
            $table->boolean('status')->default(false);
            $table->string('profile_picture')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('garbage_category_id')->references('id')->on('garbage_categories');
            $table->foreign('hamlet_id')->references('id')->on('hamlets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
