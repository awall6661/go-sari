<?php

use App\Models\GarbageCategory;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = [
            'category_name' => 'Rumah Tangga Umum',
            'price' => 30000,
            'created_at' => now(),
            'updated_at' => now()
        ];
        GarbageCategory::insert($record);
    }
}
