<?php

use App\Models\Admin;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            'name' => 'Super Admin',
            'phone'=> '6280111222333',
            'email'=> 'superadmin@gosari.com',
            'email_verified_at'=> date('Y-m-d H:i:s'),
            'role'=> 'superadmin',
            'password'=> Hash::make('password'),
            'remember_token' => Str::random(16),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
        Admin::insert($records);
    }
}
