<?php

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = [
            'name' => 'Joni',
            'email' => 'joni@email.com',
            'password' => Hash::make('password'),
            'nik' => '1111222233334444',
            'phone' => '62811223444222',
            'garbage_category_id' => 1,
            'garbage_can_location' => 'Belakang',
            'garbage_can_picture' => 'example-trash.jpg',
            'hamlet_id' => 1,
            'rt' => '1',
            'address' => 'Go Sari RT 1',
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        User::insert($record);
    }
}
