<?php

namespace App\Exports;

use App\Models\Invoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $year = date('Y');
        $invoices = Invoice::where('payment_status', Invoice::PAID)
            ->where('month', 'like', '%' . $year)
            ->join('users', 'users.id', '=', 'invoices.user_id')
            ->get([
                'payment_date',
                'name',
                'code',
                'month',
                'total_amount',
            ]);

        return $invoices;
    }

    public function headings(): array
    {
        return [
            'Tanggal Transaksi',
            'Nama',
            'Kode Tagihan',
            'Bulan',
            'Total Tagihan',
        ];
    }
}
