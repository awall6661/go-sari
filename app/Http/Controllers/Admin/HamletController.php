<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Hamlet;
use App\Models\TargetMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class HamletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hamlets = Hamlet::all();
        return view('admin.hamlets.index', compact('hamlets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hamlets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use($request) {
            $hamlet = Hamlet::create([
                'hamlet_name' => $request->hamlet_name
            ]);

            TargetMember::create([
                'hamlet_id' => $hamlet->id,
                'target_qty' => $request->target_qty
            ]);
        });
        
        return redirect()->route('hamlets.index')->with('success', 'Data berhasil disimpan.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hamlet = Hamlet::find($id);
        return view('admin.hamlets.edit', compact('hamlet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $hamlet = Hamlet::find($id);
            
            $hamlet->hamlet_name = $request->hamlet_name;
            $hamlet->save();
        
        return redirect()->route('hamlets.index')->with('success', 'Data berhasil disimpan.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use($id){
            $targetMember = TargetMember::where('hamlet_id', $id)->first();
            $hamlet = Hamlet::find($id);

            $targetMember->delete();
            $hamlet->delete();
        });
        
        return redirect()->route('hamlets.index')->with('success', 'Data berhasil dihapus.');
    }
}
