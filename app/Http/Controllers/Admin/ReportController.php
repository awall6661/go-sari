<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ReportExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hamlet;
use App\Models\Invoice;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends Controller
{
    public function index()
    {
        $months = Controller::getIndoMonth();
        $hamlets = Hamlet::all();
        $m = date('m');
        $currentMonth = Controller::formatMonthIndo($m);

        return view('admin.reports.index', compact('months', 'hamlets', 'currentMonth'));
    }

    public function getData(Request $request)
    {
        $status = $request->status;
        \DB::statement("SET SQL_MODE=''");

        $filterMonth = $request->month . " " . date("Y");
        if ($request->month == "all") {
            $data = Invoice::where('payment_status', Invoice::PAID)
            ->join('users', 'users.id', '=', 'invoices.user_id')
            ->join('hamlets', 'hamlets.id', '=', 'users.hamlet_id')
            ->join('garbage_categories', 'garbage_categories.id', '=', 'users.garbage_category_id')
            ->select(array(
                'invoices.id',
                'invoices.month',
                'payment_date',
                'name',
                'hamlet_name',
                'category_name',
                'total_amount',
                DB::raw("SUM(invoices.total_amount) OVER() AS total")
            ))->get();
        } else {
            $data = Invoice::where('payment_status', Invoice::PAID)
            ->where('month', $filterMonth)
            ->join('users', 'users.id', '=', 'invoices.user_id')
            ->join('hamlets', 'hamlets.id', '=', 'users.hamlet_id')
            ->join('garbage_categories', 'garbage_categories.id', '=', 'users.garbage_category_id')
            ->select(array(
                'invoices.id',
                'invoices.month',
                'payment_date',
                'name',
                'hamlet_name',
                'category_name',
                'total_amount',
                DB::raw("SUM(invoices.total_amount) OVER() AS total")
            ))->get();
        }
        
        return response()->json([
            'data' => $data
        ]);
    }

    public function exportReport()
    {
        return Excel::download(new ReportExport, 'Laporan Transaksi '.date('Y').'.xlsx');
    }
}
