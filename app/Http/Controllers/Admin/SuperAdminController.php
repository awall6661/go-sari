<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SuperAdminController extends Controller
{
    public function indexAdmins()
    {
        $admins = Admin::all();

        return view('super-admin.admins.index', compact('admins'));
    }

    public function createAdmin()
    {
        return view('super-admin.admins.create');
    }

    public function storeAdmin(Request $request)
    {
        Admin::insert([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => '+'.$request->phone,
            'role' => $request->role,
            'password' => Hash::make($request->password)
        ]);

        return redirect('super-admin/admins');
    }

    public function destroyAdmin($id)
    {
      $admin = Admin::find($id);
      $admin->delete();

      return response()->json([
          'success' => true,
          'message' => 'Akun berhasil dihapus.'
      ], 200);
    }
}
