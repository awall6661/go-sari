<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Hamlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function indexMembers()
    {
        return view('admin.members.index');
    }

    public function getMembersData(Request $request)
    {
        $status = $request->status;

        if ($status == "all") {
            $data = User::all();
        } elseif ($status == "active") {
            $data = User::where('status', User::ACTIVE)->get();
        } else {
            $data = User::where('status', User::INACTIVE)->get();
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function countMembers()
    {
      $all = User::count();
      $active = User::where('status', 1)->count();
      $inactive = User::where('status', 0)->count();

      return response()->json([
        'all' => $all,
        'active' => $active,
        'inactive' => $inactive,
      ], 200);
    }
    
    public function getMemberDetails($id)
    {          
        $user = User::find($id);
        $hamlet = Hamlet::where('id', $user->hamlet_id)->first();

        return view ('admin.members.details', compact('user', 'hamlet'));
    }

    public function changeMemberStatus(Request $request)
    {
        $userId = $request->id;

        $member = User::find($userId);
        $member->status = $request->status;
        $member->save();

        return response()->json([
            'success' => true,
            'message' => 'Status member berhasil dirubah.',
        ], 200);
    }

    // public function destroyMember(Request $request)
    // {
    //     $member = User::where('id', $request->id);
    //     $member->delete();

    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Status member berhasil dirubah.',
    //     ], 200);
    // }

    public function markNotification(Request $request)
    {
        $user = Auth::guard('admin')->user();

        $user->unreadNotifications
            ->when($request->input('id'), function ($query) use ($request) {
                return $query->where('id', $request->input('id'));
            })
        ->markAsRead();
           
        return response()->json(['message' => 'Notifikasi telah dibaca.']);
    }
}
