<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Hamlet;
use App\Models\Invoice;
use App\Models\TargetMember;
use Illuminate\Http\Request;
use App\User;
use DB;

class AdminDashboardController extends Controller
{
    public function index()
    {
        // invoice summary
        $m = date('m');
        $year  = date('Y');
        $invoiceMonth = Controller::formatMonthIndo($m) . ' ' . $year;

        $invoicePaid = Invoice::where('month', $invoiceMonth)->where('payment_status', Invoice::PAID)->count();
        $invoiceUnpaid = Invoice::where('month', $invoiceMonth)->where('payment_status', Invoice::UNPAID)->count();
        $invoicePending = Invoice::where('month', $invoiceMonth)->where('payment_status', Invoice::PENDING)->count();
        $invoiceTotal = Invoice::where('month', $invoiceMonth)->count();

        $paidAmount = Invoice::where('payment_status', Invoice::PAID)->where('month', $invoiceMonth)->sum('total_amount');
        $unpaidAmount = Invoice::where('payment_status', '!=', Invoice::PAID)->where('month', $invoiceMonth)->sum('total_amount');

        // Hamlet members and target
        \DB::statement("SET SQL_MODE=''");
        $hamletMembersCount = DB::table('hamlets')
            ->join('target_members', 'target_members.hamlet_id', '=', 'hamlets.id')
            ->leftJoin('users', 'users.hamlet_id', '=', 'hamlets.id')
            ->select(array('hamlets.id', 'hamlet_name', 
                DB::raw("COUNT(users.id) AS total_member, target_members.target_qty,
                COUNT(users.id) / target_members.target_qty * 100 AS percentage")))
            ->groupBy('hamlets.id')
            ->get();
        $totalMembers = User::count();

        $vars = array(
            'invoiceMonth', 
            'invoicePaid', 
            'invoiceUnpaid', 
            'invoicePending', 
            'invoiceTotal', 
            'paidAmount', 
            'unpaidAmount', 
            'hamletMembersCount',
            'totalMembers'
        );
        
        return view('admin.dashboard', compact($vars));
    }
}
