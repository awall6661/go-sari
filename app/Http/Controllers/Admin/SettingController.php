<?php

namespace App\Http\Controllers\Admin;

use App\Bank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    // setting bank account
    public function indexBank()
    {
        $data['banks'] = Bank::all();

        $data['menu'] = 'Akun Bank';
        $data['page'] = 'Data Akun Bank';
        $data['url'] = '/admin/setting/banks';

        return view('admin/settings/bank/index', $data);
    }

    public function createBank()
    {
        $data['menu'] = 'Akun Bank';
        $data['page'] = 'Tambah Akun Bank';
        $data['url'] = '/admin/setting/banks';
        
        return view('admin/settings/bank/create', $data);
    }

    public function storeBank(Request $request)
    {        
        Bank::create([
            'name' => $request->name,
            'number' => $request->number,
            'owner' => $request->owner,
            'status' => $request->status,
        ]);

        return redirect()->route('setting.banks.index');
    }

    public function editBank($id)
    {
        $bank = Bank::find($id);

        $data['menu'] = 'Akun Bank';
        $data['page'] = 'Edit Akun Bank';
        $data['url'] = '/admin/setting/banks';
        
        return view('admin/settings/bank/edit', $data, compact('bank'));
    }

    public function updateBank(Request $request, $id)
    {
        $bank = Bank::find($id);
        
        $bank->name = $request->name;
        $bank->number = $request->number;
        $bank->owner = $request->owner;
        $bank->status = $request->status;
        $bank->save();

        return redirect()->route('setting.banks.index');
    }

    public function destroyBank($id)
    {
        $bank = Bank::find($id);
        $bank->delete();

        return redirect()->route('setting.banks.index');
    }
}
