<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Notification;
use App\Models\Hamlet;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\TransactionConfirmedNotification;

class TransactionController extends Controller
{
    public function index()
    {
        $months = Controller::getIndoMonth();
        $hamlets = Hamlet::all();

        return view('admin.transactions.index', compact('months', 'hamlets'));
    }

    public function getData(Request $request)
    {
        $data = Invoice::where('payment_status', Invoice::PAID)
            ->join('users', 'users.id', '=', 'invoices.user_id')
            ->join('hamlets', 'hamlets.id', '=', 'users.hamlet_id')
            ->join('garbage_categories', 'garbage_categories.id', '=', 'users.garbage_category_id')
            ->get([
                'invoices.id',
                'month',
                'name',
                'hamlet_name',
                'rt',
                'category_name',
                'total_amount',
                'payment_status',
                'payment_receipt',
            ]);
        //
        $status = $request->status;
        if ($status == "processed") {
            $data = Invoice::where('payment_status', Invoice::PAID)
                ->join('users', 'users.id', '=', 'invoices.user_id')
                ->join('hamlets', 'hamlets.id', '=', 'users.hamlet_id')
                ->join('garbage_categories', 'garbage_categories.id', '=', 'users.garbage_category_id')
                ->get([
                    'invoices.id',
                    'month',
                    'name',
                    'hamlet_name',
                    'rt',
                    'category_name',
                    'total_amount',
                    'payment_status',
                    'payment_receipt',
                ]);
        } elseif ($status == "unprocessed") {
            $data = Invoice::where('payment_status', Invoice::PENDING)
                ->join('users', 'users.id', '=', 'invoices.user_id')
                ->join('hamlets', 'hamlets.id', '=', 'users.hamlet_id')
                ->join('garbage_categories', 'garbage_categories.id', '=', 'users.garbage_category_id')
                ->get([
                    'invoices.id',
                    'month',
                    'name',
                    'hamlet_name',
                    'rt',
                    'category_name',
                    'total_amount',
                    'payment_status',
                    'payment_receipt',
                ]);
        } else {
            $data = Invoice::where('payment_status', '!=', Invoice::UNPAID)
                ->join('users', 'users.id', '=', 'invoices.user_id')
                ->join('hamlets', 'hamlets.id', '=', 'users.hamlet_id')
                ->join('garbage_categories', 'garbage_categories.id', '=', 'users.garbage_category_id')
                ->get([
                    'invoices.id',
                    'month',
                    'name',
                    'hamlet_name',
                    'rt',
                    'category_name',
                    'total_amount',
                    'payment_status',
                    'payment_receipt',
                ]);
        }


        return response()->json([
            'data' => $data
        ]);
    }

    public function count()
    {
        $all = Invoice::where('payment_status', '!=', Invoice::UNPAID)->count();
        $processed = Invoice::where('payment_status', Invoice::PAID)->count();
        $unprocessed = Invoice::where('payment_status', Invoice::PENDING)->count();

        return response()->json([
            'all' => $all,
            'processed' => $processed,
            'unprocessed' => $unprocessed,
        ]);
    }

    public function changeStatus(Request $request)
    {
        $invoice = Invoice::find($request->id);
        \DB::transaction(function () use($invoice, $request) { 
            $invoice->payment_status = Invoice::PAID;
            $invoice->save();
            
            // Push notification to user
            $invoice = Invoice::find($request->id);
            $user = User::where('id', $invoice->user_id)->first();
            Notification::send($user, new TransactionConfirmedNotification($invoice));
        });

        return response()->json([
            'success' => true,
            'message' => 'Status pembayaran berhasil dirubah.',
        ], 200);
    }
}
